﻿namespace WindowsForm
{
    /*
     * Trapper class, subclass of Hunter. 
     */
    class Trapper : Hunter
    {
        public Trapper(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Trapper";
        }
    }
}
