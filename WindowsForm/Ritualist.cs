﻿namespace WindowsForm
{
    /*
    * Ritualist class, subclass of Shaman. 
    */
    class Ritualist : Shaman
    {
        public Ritualist(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Ritualist";
        }
    }
}
