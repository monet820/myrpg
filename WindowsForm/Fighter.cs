﻿namespace WindowsForm
{
    // Class Fighter
    class Fighter : Character
    {
        public Fighter(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Fighter";
        }
    }
}
