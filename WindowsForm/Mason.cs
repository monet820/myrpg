﻿namespace WindowsForm
{
    /*
    * Mason class, subclass of Builder. 
    */
    class Mason : Builder
    {
        public Mason(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Mason";
        }
    }
}
