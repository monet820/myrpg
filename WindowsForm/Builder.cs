﻿namespace WindowsForm
{
    // Class Builder
    class Builder : Character
    {
        public Builder(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "BUILDER";
        }
    }
}
