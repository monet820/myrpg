﻿namespace WindowsForm
{
    // Class Hunter.
    class Hunter : Character
    {

        public Hunter(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {

        }

        public override string GetIntroduction()
        {
            return "Hunter";
        }
    }
}
