﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace WindowsForm
{
    public partial class Form1 : Form
    {
        private readonly string ConnectionString = "Server=PC7265\\SQLEXPRESS;Database=DatabaseRPG;Trusted_Connection=True;";

        private Character NewCharacter;

        private List<Character> CharacterList = new List<Character>();

        private string NewName = "";

        private int SubClassID;

        public Form1()
        {
            InitializeComponent();
        }

        // Return true/false if namecheck pass.
        private bool ValidName()
        {
            bool validName = false;

            if (this.NewName.Length < 4)
            {
                MessageBox.Show("You need to have atleast 4 characters in the name");
            }
            else if (!Regex.IsMatch(this.NewName, @"^[a-zA-Z]+$"))
            {
                MessageBox.Show("The name accepts only alphabetical characters.");
            }
            else
            {
                validName = true;
            }

            return validName;
        }

        // Return true if subclass has been selected
        public bool ValidCharacter()
        {
            bool validCharacter = false;
            if (NewCharacter == null)
            {
                MessageBox.Show("You need to select a valid subclass from the list to the right.");
            }
            if (NewCharacter != null)
            {
                validCharacter = true;
            }
            return validCharacter;
        }


        // Add the newCharacter to the Database.
        private void CreateNewCharacter()
        {
            try
            {
                if (CharacterSelection.SelectedItems.Count == 0)
                {
                    MessageBox.Show("You need to click on a class from the list to the right, of the one you want to create.");
                }
                if (CharacterSelection.SelectedItems.Count != 0)
                {
                    if (ValidName() == true && ValidCharacter() == true)
                    {
                        this.NewCharacter.Name = this.NewName;

                        using (SqlConnection connection = new SqlConnection(ConnectionString))
                        {
                            connection.Open();
                            string sql = "INSERT INTO Player (Name, SubClassID) VALUES (@Name, @SubClassID)";

                            using (SqlCommand command = new SqlCommand(sql, connection))
                            {
                                command.Parameters.AddWithValue("@Name", this.NewCharacter.Name.ToString());
                                command.Parameters.AddWithValue("@SubClassID", this.SubClassID);
                                command.ExecuteNonQuery();

                                // Telling the user that the character was created.
                                MessageBox.Show("Character created with name: " + NewName);

                                // Clearing name input.
                                CharacterName.Clear();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        // Get all Characters from database and add them to List of Characters.
        private void RefreshCharacterList()
        {
            try
            {
                // Clear old character list information.
                CharacterList.Clear();

                // SQL query to use.
                string sql = "SELECT * FROM Player";

                // Start new sql connection.
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    // Open connection to database.
                    connection.Open();

                    // Start new sql command.
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // Start new sql reader.
                        using (SqlDataReader reader = command.ExecuteReader())
                        {

                            // Get all items to listbox.
                            while (reader.Read())
                            {
                                string subType = reader["SubClassID"].ToString().ToLower();
                                if (subType == "1")
                                {
                                    NewCharacter = new Trapper(reader["Name"].ToString(), 45, 100, 60);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "2")
                                {
                                    NewCharacter = new Tracker(reader["Name"].ToString(), 50, 100, 50);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "3")
                                {
                                    NewCharacter = new Squire(reader["Name"].ToString(), 100, 20, 100);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "4")
                                {
                                    NewCharacter = new Shieldmaden(reader["Name"].ToString(), 100, 10, 120);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "5")
                                {
                                    NewCharacter = new Ritualist(reader["Name"].ToString(), 500, 100, 5);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "6")
                                {
                                    NewCharacter = new Medicine_man(reader["Name"].ToString(), 400, 200, 10);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "7")
                                {
                                    NewCharacter = new Mason(reader["Name"].ToString(), 500, 100, 5);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "8")
                                {
                                    NewCharacter = new Conjourer(reader["Name"].ToString(), 500, 100, 5);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "9")
                                {
                                    NewCharacter = new Carpenter(reader["Name"].ToString(), 300, 50, 50);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                                if (subType == "10")
                                {
                                    NewCharacter = new Blacksmith(reader["Name"].ToString(), 300, 100, 300);
                                    NewCharacter.ID = Int32.Parse(reader["ID"].ToString());
                                    CharacterList.Add(NewCharacter);
                                }
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        // Remove selected item from table.
        private void DeleteFromTable()
        {
            try
            {
                if (listBox1.SelectedItem == null)
                {
                    MessageBox.Show("You need pick an item from the character list to the left, if there are no item you need to refresh the list.");
                }
                if (listBox1.SelectedItem != null)
                {

                    // Get information about the item to remove from table.
                    Character curItem = (Character)listBox1.SelectedItem;

                    using (var sc = new SqlConnection(ConnectionString))
                    {
                        using (var cmd = sc.CreateCommand())
                        {
                            sc.Open();
                            cmd.CommandText = "DELETE FROM Player WHERE ID = @ID;";
                            cmd.Parameters.AddWithValue("@ID", curItem.ID);
                            cmd.ExecuteNonQuery();
                        }
                        // User feedback, delete successfull.
                        MessageBox.Show("Character: " + curItem.Name + " Was deleted.");
                    }
                }
            }
            // Catch exception if try block fails.
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        // Fill LabelBox with all the rows from table Player.
        private void PrintToLabelBox()
        {
            try
            {
                // Clear old listbox information.
                listBox1.Items.Clear();

                foreach (var item in CharacterList)
                {
                    listBox1.Items.Add(item);
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CharacterSelection_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        // Get NewCharacter properties.
        private void GetClassInformation()
        {
            LabelName.Text = NewCharacter.Name;
            LabelHitPoints.Text = NewCharacter.HitPoints.ToString();
            LabelResource.Text = NewCharacter.Resource.ToString();
            LabelArmorRating.Text = NewCharacter.ArmorRating.ToString();
        }

        // Eventhandler for what item is selected on the Listview.
        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CharacterSelection.SelectedItems.Count == 0)
                    return;
                ListViewItem item = CharacterSelection.SelectedItems[0];

                if (item.Text.ToLower() == "trapper")
                {
                    NewCharacter = new Trapper(NewName, 45, 100, 60);
                    SubClassID = 1;
                }
                else if (item.Text.ToLower() == "tracker")
                {
                    NewCharacter = new Tracker(NewName, 50, 100, 50);
                    SubClassID = 2;
                }
                else if (item.Text.ToLower() == "squire")
                {
                    NewCharacter = new Squire(NewName, 100, 20, 100);
                    SubClassID = 3;
                }
                else if (item.Text.ToLower() == "shieldmaden")
                {
                    NewCharacter = new Shieldmaden(NewName, 100, 10, 120);
                    SubClassID = 4;
                }
                else if (item.Text.ToLower() == "ritualist")
                {
                    NewCharacter = new Ritualist(NewName, 500, 100, 5);
                    SubClassID = 5;
                }
                else if (item.Text.ToLower() == "medicine man")
                {
                    NewCharacter = new Medicine_man(NewName, 400, 200, 10);
                    SubClassID = 6;
                }
                else if (item.Text.ToLower() == "mason")
                {
                    NewCharacter = new Mason(NewName, 500, 100, 5);
                    SubClassID = 7;
                }
                else if (item.Text.ToLower() == "conjourer")
                {
                    NewCharacter = new Conjourer(NewName, 500, 100, 5);
                    SubClassID = 8;
                }
                else if (item.Text.ToLower() == "carpenter")
                {
                    NewCharacter = new Carpenter(NewName, 300, 50, 50);
                    SubClassID = 9;
                }
                else if (item.Text.ToLower() == "blacksmith")
                {
                    NewCharacter = new Blacksmith(NewName, 300, 100, 300);
                    SubClassID = 10;
                }
                // Get information about the class that was created.
                GetClassInformation();

                // Fill the text box
                CharacterInformation.Clear();
                // Call to get class introduction.
                CharacterInformation.AppendText(NewCharacter.GetIntroduction());


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void CreateNewCharacter_Click(object sender, EventArgs e)
        {
            CreateNewCharacter();
        }


        // Setting new character name to textfield input.
        private void CharacterName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                // Setting name to input.
                this.NewName = CharacterName.Text.Trim();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Get all characters from table and add them to the list of Characters.
                RefreshCharacterList();

                // Get all rows from table and print them to the labelbox.
                PrintToLabelBox();
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        // Button to delete an character from the database.
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DeleteFromTable();
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Character curItem = (Character)listBox1.SelectedItem;
            NewCharacter = curItem;
            GetClassInformation();
        }

        private void RenameButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidName() == true)
                {
                    if (listBox1.SelectedItem == null)
                    {
                        MessageBox.Show("You need pick an item from the character list to the left, if there are no item you need to refresh the list.");
                    }
                    if (listBox1.SelectedItem != null)
                    {

                        // Get information about the item to rename.
                        Character curItem = (Character)listBox1.SelectedItem;

                        using (var sc = new SqlConnection(ConnectionString))
                        using (var cmd = sc.CreateCommand())
                        {
                            sc.Open();
                            cmd.CommandText = "UPDATE Player SET Name = @UpdateName WHERE ID = @ID; ";
                            cmd.Parameters.AddWithValue("@ID", curItem.ID);
                            cmd.Parameters.AddWithValue("@UpdateName", this.NewName);
                            cmd.ExecuteNonQuery();
                        }
                        // User feedback, rename successfull.
                        MessageBox.Show("Character: " + curItem.Name + " Was renamed to " + this.NewName);
                    }

                    // Clearing name input.
                    CharacterName.Clear();
                }
            }
            // Catch exception, if rename does not work.
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AttackButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.SelectedItem == null)
                {
                    MessageBox.Show("You need pick an item from the character list to the left, if there are no item you need to refresh the list.");
                }
                if (listBox1.SelectedItem != null)
                {
                    // Get information about the item to rename.
                    Character curItem = (Character)listBox1.SelectedItem;

                    curItem.Attack();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MoveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.SelectedItem == null)
                {
                    MessageBox.Show("You need pick an item from the character list to the left, if there are no item you need to refresh the list.");
                }
                if (listBox1.SelectedItem != null)
                {
                    // Get information about the item to rename.
                    Character curItem = (Character)listBox1.SelectedItem;

                    curItem.Move();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
