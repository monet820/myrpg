﻿namespace WindowsForm
{
    // Class Shaman.
    class Shaman : Character
    {
        public Shaman(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Shaman";
        }
    }
}
