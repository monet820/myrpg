﻿namespace WindowsForm
{
    /*
    * Squire class, subclass of Fighter. 
    */
    class Squire : Fighter
    {
        public Squire(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Squire";
        }
    }
}
