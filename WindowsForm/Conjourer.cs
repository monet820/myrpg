﻿namespace WindowsForm
{
    /*
    * Conjourer class, subclass of Shaman. 
    */
    class Conjourer : Shaman
    {
        public Conjourer(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Conjourer";
        }
    }
}
