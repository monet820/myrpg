﻿namespace WindowsForm
{
    /*
     * Class Carpenter subclass of Builder
     */
    class Carpenter : Builder

    {
        public Carpenter(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Carpenter";
        }

    }
}
