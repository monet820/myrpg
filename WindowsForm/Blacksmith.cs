﻿namespace WindowsForm
{
    /*
     * Class Blacksmith subclass of Builder
     */
    class Blacksmith : Builder
    {
        public Blacksmith(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Blacksmith";
        }
    }
}
