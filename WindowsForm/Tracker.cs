﻿namespace WindowsForm
{
    /*
    * Tracker class, subclass of Hunter. 
    */
    class Tracker : Hunter
    {
        public Tracker(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Tracker";
        }
    }
}
