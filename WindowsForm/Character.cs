﻿using System.Windows.Forms;

namespace WindowsForm
{
    /* Class Character
     * To be Player created Characters.
     */
    abstract class Character
    {
        // Primary Key
        public int ID { get; set; }

        // Field values
        public string Name { get; set; }
        public int Resource { get; set; }
        public int HitPoints { get; set; }
        public int ArmorRating { get; set; }

        // Constructor, set Name, Resource, HitPoints and ArmorRating.
        public Character(string Name, int Resource, int HitPoints, int ArmorRating)
        {
            this.Name = Name;
            this.Resource = Resource;
            this.HitPoints = HitPoints;
            this.ArmorRating = ArmorRating;
        }

        // Perform Attack.
        public void Attack()
        {
            MessageBox.Show(Name + " Attacked");
        }
        // Perform Move.
        public void Move()
        {
            MessageBox.Show(Name + " Moved");
        }

        // Get Character information.
        public virtual string GetIntroduction()
        {
            return "";
        }
    }
}
