﻿namespace WindowsForm
{
    /*
    * Shieldmaden class, subclass of Fighter. 
    */
    class Shieldmaden : Fighter
    {
        public Shieldmaden(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "Shieldmaden";
        }

    }
}
