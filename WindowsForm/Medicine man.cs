﻿namespace WindowsForm
{
    /*
    * Medicine man class, subclass of Shaman. 
    */
    class Medicine_man : Shaman
    {
        public Medicine_man(string Name, int Resource, int HitPoints, int ArmorRating) : base(Name, Resource, HitPoints, ArmorRating)
        {
        }

        public override string GetIntroduction()
        {
            return "MEDICINE MAN";
        }
    }
}
